function Champion() {
    this.build = {
        abilities : {},
        level : 1
    };
}

Champion.prototype = {
    save : function () {
        var data = {
            code : null,
            abilities : [],
            level : this.build.level
        };

        if (this.code) {
            data.code = this.code;
        }

        $.each(this.build.abilities, function(key, value){
            data.abilities.push({
                "code" : key,
                "levels" : value.slice(0)
            });
        });

        return data;
    },

    setData : function (data) {
        // by reference or by value should not matter at this point, should be considered read-only.
        for (var property in data) {
            this[property] = data[property];
        }
    },

    getDescription : function () {
        return this.description;
    },

    getCode : function () {
        return this.code;
    },

    /**
     * Remove an ability from the build
     * @param ability
     * @param position
     */
    removeBuildAbility : function (ability, position) {
        position = parseInt(position, 10);

        if (this.build.abilities.hasOwnProperty(ability)) {
            for (var i = 0; i < this.build.abilities[ability].length; i++) {
                if (this.build.abilities[ability][i] === position) {
                    this.build.abilities[ability].splice(i, 1);
                }
            }
        }
    },

    /**
     * Adds an ability point to the abilities build.
     * @param ability
     * @param position
     */
    addBuildAbility : function (ability, position) {
        position = parseInt(position, 10);

        // throws an exception if the ability does not exist.
        var abilityData = this.getAbilityByCode(ability);

        if (typeof(this.build.abilities[ability]) === "undefined") {
            this.build.abilities[ability] = [];
        }

        // clone the build for validation, apply the change, validate
        var clone = this.cloneBuild();
        clone[ability].push(position);

        // sorting is important for validation!!
        clone[ability].sort(function (a, b) {
            return a < b ? -1 : 1;
        });
        var isValid = this.validateBuild(clone, position, abilityData);

        if (isValid) {
            this.build.abilities[ability].push(position);
            this.build.abilities[ability].sort(function (a, b) {
                return a < b ? -1 : 1;
            });
        }

        return isValid;
    },

    /**
     * Clone a build
     * @returns {{}}
     */
    cloneBuild : function () {
        var clone = {};
        for (var prop in this.build.abilities) {
            clone[prop] = this.build.abilities[prop].slice(0);
        }
        return clone;
    },

    /**
     * Validate a build by the LoL in-game rules
     * @param build
     * @param position
     * @returns {boolean}
     */
    validateBuild : function (build, position, newAbilityData) {
        if (typeof(position) !== "number") {
            throw {
                name : "InvalidPositionValue",
                message : "Number expected but other type found."
            };
        }

        function EvolutionsSortCompareFn(a, b) {
            return a < b ? -1 : 1;
        }

        // innate abilities should not have points
        for (var ability in build) {
            if (build.hasOwnProperty(ability)) {
                var abilityData = this.getAbilityByCode(ability);
                if (abilityData.type === "passive" && build[ability].length > 0) {
                    log("Passive ability with points found");
                    return false;
                }

                if (abilityData.archetype === "basic" && build[ability].length > 0) {
                    for (var i = 0; i < build[ability].length; i++) {
                        var abilityLevel = i + 1;
                        var championLevel = parseInt(build[ability][i], 10);

                        // ability level may not exceed half the champion level rounded up
                        if (abilityLevel > Math.ceil(championLevel / 2)) {
                            log("Ability level max exceeded: alvl=" + abilityLevel + " max=" + Math.ceil(championLevel / 2) + " clvls: " + build[ability]);
                            return false;
                        }
                    }
                }

                if (abilityData.archetype === "basic" && build[ability].length > 0) {
                    if (build[ability].length > 5) {
                        return false;
                    }
                }

                if (abilityData.archetype === "ultimate" && build[ability].length > 0) {
                    if (build[ability].length > 3) {
                        return false;
                    }

                    for (var k = 0; k < build[ability].length; k++) {
                        cLvl = parseInt(build[ability][k], 10);
                        if ((0 === k && cLvl < 6) ||
                            (1 === k && cLvl < 11) ||
                            (2 === k && cLvl < 16)) {
                            return false;
                        }
                    }
                }

                if (abilityData.type === "evolution" && build[ability].length > 0) {
                    if (build[ability].length > 1) {
                        return false;
                    }

                    // aggregate the evolutions
                    var evolutions = [];
                    for (var evo in build) {
                        if (this.getAbilityByCode(evo).type === "evolution") {
                            evolutions = evolutions.concat(build[evo]);
                        }
                    }
                    evolutions.sort(EvolutionsSortCompareFn);

                    /**
                     * can only have 3 evolutions max
                     * Kha'Zix can gain a fourth evolution point through the The Hunt Is On! event.
                     * http://leagueoflegends.wikia.com/wiki/The_Hunt_Is_On!
                     */
                    if (evolutions.length > 3) {
                        log("Max evolutions exceeded.");
                        return false;
                    }

                    for (var evoIdx = 0; evoIdx < evolutions.length; evoIdx++) {
                        ecLvl = parseInt(evolutions[evoIdx], 10);
                        if ((0 === evoIdx && ecLvl < 6) ||
                            (1 === evoIdx && ecLvl < 11) ||
                            (2 === evoIdx && ecLvl < 16)
                            ) {
                            log("Evolution level max exceeded.");
                            return false;
                        }
                    }
                }

                // champion level already taken
                for (var j = 0; j < this.build.abilities[ability].length; j++) {
                    var chLvl = parseInt(this.build.abilities[ability][j], 10);
                    if (chLvl === position && abilityData.type !== "evolution" && newAbilityData.type !== "evolution") {
                        return false;
                    }
                }
            }
        }

        return true;
    },

    getAbilityByCode : function (abilityCode) {
        for (var i = 0; i < this.abilities.length; i++) {
            if (this.abilities[i].code === abilityCode) {
                return this.abilities[i];
            }
        }

        throw {
            name : 'AbilityNotFound',
            message : "Champion ability not found: " + abilityCode
        };
    }
};