function ChampionSpells() {
    this.spells = [null, null];
}

ChampionSpells.prototype = {
    save : function() {
        return this.spells.slice(0);
    },

    load : function(data) {
        if ($.isArray(data)) {
            this.spells = data.slice(0,2);
        }
    },

    setSpell : function (number, summonerSpellCode) {
        var index = (parseInt(number, 10));
        summonerSpellCode = summonerSpellCode.toLowerCase();

        if (0 > index || index > 1) {
            throw {
                name : "InvalidSummonerSpellSlotNumber",
                message : "Expected the value 0 or 1."
            };
        }

        for (var i in this.spells) {
            if (i !== index) {
                if (this.spells[i] === summonerSpellCode) {
                    // spell already exists in the other slot
                    return false;
                }
            }
        }

        this.spells[index] = summonerSpellCode;
        return true;
    },

    getSpells : function () {
        return this.spells.slice();
    }
};