function ItemSet() {
    this.items = [null, null, null, null, null, null];
    this.title = "...";
    this.currentItemIndex = 0;
}

function BuildItem(itemCode) {
    this.code = itemCode;
    this.amount = 1;
}

ItemSet.prototype = {
    isValidIndex: function (index) {
        if (index > 5) {
            throw {
                name: "InvalidItemIndex",
                message: "Index should be between 0 and 5."
            };
        }
    },

    ensureValidIndex: function (index) {
        if (index > 5) {
            return 5;
        } else if (index < 0) {
            return 0;
        } else {
            return index;
        }
    },

    getItemData: function (itemCode) {
        var itemData = $.fn.LeaguePress.findByCode("items", itemCode);
        if (null == itemCode) {
            throw {
                name: "UnknownItem",
                message: "Item not found: " + itemCode
            };
        }

        return itemData;
    },

    addItem: function (itemCode) {
        this.addItemAtIndex(itemCode, this.getCurrentItemIndex());
        this.currentItemIndex++;
    },

    addItemAtIndex: function (itemCode, index, amount) {
        this.isValidIndex(index);
        var itemData = this.getItemData(itemCode);

        if ("undefined" !== typeof amount &&
            index < 6 &&
            null !== itemData) {

            this.items[index] = new BuildItem(itemCode);
            if ("undefined" !== typeof itemData.amountPerSlot) {
                this.items[index].isMultiple = true;

                if (amount <= itemData.amountPerSlot) {
                    this.items[index].amount = amount;
                }
            }
        } else if (null !== this.items[index] &&
            itemCode === this.items[index].code &&
            "undefined" !== typeof itemData.amountPerSlot) {
            if (this.items[index].amount < itemData.amountPerSlot) {
                this.items[index].amount++;
            }
        } else {
            this.items[index] = new BuildItem(itemCode);

            if ("undefined" !== typeof itemData.amountPerSlot &&
                itemData.amountPerSlot > 1) {
                this.items[index].isMultiple = true;
            }
        }
    },

    removeItemAtIndex: function (index) {
        this.isValidIndex(index);

        if (null != this.items[index] &&
            this.items[index].amount > 1) {
            this.items[index].amount--;
        } else {
            this.items[index] = null;
        }
    },

    getCurrentItemIndex: function () {
        if (this.currentItemIndex < 0 || this.currentItemIndex > 5) {
            return this.currentItemIndex = 5;
        }

        return this.currentItemIndex;
    }
};

function ItemsBuild() {
    this.itemSets = [];
    this.itemSets.push(new ItemSet());
    this.currentSetIndex = 0;
}

ItemsBuild.prototype = {
    save: function () {
        var data = [];

        $.each(this.itemSets, function (itemSetIndex, itemSet) {
            data[itemSetIndex] = {
                title: itemSet.title,
                items: []
            };
            $.each(itemSet.items, function (itemIndex, item) {
                if (item) {
                    data[itemSetIndex].items.push(
                        { "code": item.code, "amount": item.amount });
                }
            });
        });

        return data;
    },

    getCurrentItemSet: function () {
        if (0 === this.itemSets.length) {
            this.itemSets.push(new ItemSet());
            return 0;
        }

        while (this.currentSetIndex >= 0) {
            if ("undefined" === this.itemSets[this.currentSetIndex]) {
                this.currentSetIndex--;
            } else {
                return this.currentSetIndex;
            }
        }

        this.currentSetIndex = 0;
        if ("undefined" !== typeof this.itemSets[0]) {
            this.itemSets[0] = new ItemSet();
        }
        return this.currentSetIndex;
    },

    addItemSet: function (items) {
        var itemset = new ItemSet();
        if ($.isArray(items)) {
            $.each(items, function (index, value) {
                if (index < 6) {
                    itemset.addItemAtIndex(value.code, index, value.amount);
                }
            });
        }
        this.itemSets.push(itemset);

        return itemset;
    },

    removeItemSet: function (itemSetIndex) {
        this.itemSets.splice(itemSetIndex, 1);
    },

    reorderItemSets: function (newOrder) {
        var newItemSets = [];
        for (var i = 0; i < newOrder.length; i++) {
            var itemSetIndex = newOrder[i];
            if ("undefined" !== typeof this.itemSets[itemSetIndex]) {
                newItemSets.push(this.itemSets.splice(itemSetIndex, 1));
            }
        }

        // add remaining although there should not be any.
        for (var j = 0; j < this.itemSets.length; j++) {
            if ("undefined" !== typeof this.itemSets[j]) {
                newItemSets.push(this.itemSets[j]);
            }
        }

        this.itemSets = newItemSets;
    },

    addItem: function (itemCode) {
        var itemSet = this.itemSets[this.getCurrentItemSet()];
        itemSet.addItem(itemCode);
    },

    isValidIndex: function (itemSetIndex) {
        if ("undefined" === typeof this.itemSets[itemSetIndex]) {
            throw {
                name: "UndefinedItemSet",
                message: "Itemset does not exist."
            };
        }
    },

    addItemAtIndex: function (itemCode, itemSetIndex, itemIndex) {
        this.isValidIndex(itemSetIndex);
        this.itemSets[itemSetIndex].addItemAtIndex(itemCode, itemIndex);
    },

    removeItemAtIndex: function (itemSetIndex, itemIndex) {
        this.isValidIndex(itemSetIndex);
        this.itemSets[itemSetIndex].removeItemAtIndex(itemIndex);
    },

    setTitle: function (title, itemSetIndex) {
        this.isValidIndex(itemSetIndex);
        this.itemSets[itemSetIndex].title = title;
    }
};