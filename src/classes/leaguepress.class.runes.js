function RunesBuild() {
    this.runes = {
        "glyph": {},
        "mark": {},
        "seal": {},
        "quintessence": {}
    };
}

RunesBuild.prototype = {
    save: function () {
        var data = {};

        $.each(this.runes, function (typeIndex, typeData) {
            data[typeIndex] = [];
            $.each(typeData, function (runeCode, runeData) {
                data[typeIndex].push({
                    'code': runeCode, 'amount': runeData.amount});
            });
        });

        return data;
    },

    load: function (data) {
        var that = this;

        // clear existing.
        that.runes = {
            "glyph": {},
            "mark": {},
            "seal": {},
            "quintessence": {}
        };

        $.each(data, function (typeKey, typeRunes) {
            if (typeKey in that.runes) {
                $.each(typeRunes, function (index, rune) {
                    var runeData = $.fn.LeaguePress.findByCode("runes", rune.code);
                    if (runeData) {
                        that.runes[typeKey][rune.code] = $.extend(true, {"amount": rune.amount}, runeData);
                    }
                });
            }
        });
    },

    addRune: function (runeCode) {
        if (typeof runeCode === "undefined") {
            return;
        }

        var runeData = $.fn.LeaguePress.findByCode("runes", runeCode);
        var type = runeData.type.toLowerCase();

        if (type in this.runes) {
            var total = 0;
            for (var k in this.runes[type]) {
                //noinspection JSUnfilteredForInLoop
                total = total + this.runes[type][k].amount;
            }

            if ((type !== "quintessence" && total < 9) || (type === "quintessence" && total < 3)) {
                if (runeData.code in this.runes[type]) {
                    this.runes[type][runeData.code].amount++;
                } else {
                    this.runes[type][runeData.code] = $.extend(true, {"amount": 1}, runeData);
                }
            }
        }
    },

    removeRune: function (runeCode) {
        var runeData = $.fn.LeaguePress.findByCode("runes", runeCode);
        var type = runeData.type.toLowerCase();
        if (type in this.runes) {
            if (runeData.code in this.runes[type] &&
                this.runes[type][runeData.code].amount > 1) {
                this.runes[type][runeData.code].amount--;
            } else {
                delete(this.runes[type][runeData.code]);
            }
        }
    }
};