function MasteriesBuild() {
    this.build = {};
    this.seasonCode = "season_three";
    this.context = null;
    this.setContext();
}

MasteriesBuild.prototype = {
    save: function () {
        var data = [];

        $.each(this.build, function (key, value) {
            data.push({
                "code": key,
                "rank": value
            });
        });

        return data;
    },

    addMastery: function (masteryCode) {
        var clone = this.clone();
        if (masteryCode in this.build) {
            clone.build[masteryCode]++;
        } else {
            clone.build[masteryCode] = 1;
        }

        var validationResult = clone.validate();
        if (validationResult) {
            this.build = $.extend(true, {}, clone.build);
        }

        return validationResult;
    },

    clone: function () {
        var c = new MasteriesBuild();
        c.build = $.extend(true, {}, this.build);
        c.context = $.extend(true, {}, this.context);
        return c;
    },

    removeMastery: function (masteryCode) {
        if (masteryCode in this.build && this.build[masteryCode] > 1) {
            this.build[masteryCode]--;
        } else {
            delete(this.build[masteryCode]);
        }
    },

    validate: function () {
        var ctx = this.context = this.getContext();
        if (this.context.buildTotal > 30) {
            return false;
        }

        for (var treeIdx = 0; treeIdx < ctx.trees.length; treeIdx++) {
            for (var tierIdx = 0; tierIdx < ctx.trees[treeIdx].tiers.length; tierIdx++) {
                var masters = ctx.trees[treeIdx].tiers[tierIdx].masteries;
                for (var masteryIdx = 0; masteryIdx < masters.length; masteryIdx++) {
                    for (var k = 0; k < masters.length; k++) {
                        // rank exceeds max
                        if (masters[k].maxRank < this.build[masters[k].code]) {
                            return false;
                        }
                    }
                }
            }
        }

        return true;
    },

    getContext: function (ctx) {
        if ("undefined" === typeof ctx) {
            ctx = this.context;
        }

        // set the ranks
        ctx.buildTotal = 0;
        for (var i = 0; i < ctx.trees.length; i++) {
            ctx.trees[i].treeTotal = 0;

            for (var j = 0; j < ctx.trees[i].tiers.length; j++) {
                ctx.trees[i].tiers[j].tierTotal = 0;

                var masters = ctx.trees[i].tiers[j].masteries;
                for (var k = 0; k < masters.length; k++) {
                    if (masters[k].code in this.build) {
                        masters[k].rank = this.build[masters[k].code];

                        ctx.trees[i].tiers[j].tierTotal += this.build[masters[k].code];
                    } else {
                        masters[k].rank = 0;
                    }
                }

                ctx.trees[i].treeTotal += ctx.trees[i].tiers[j].tierTotal;
            }

            ctx.buildTotal += ctx.trees[i].treeTotal;
        }

        // round to disable masteries
        for (var treeIdx = 0; treeIdx < ctx.trees.length; treeIdx++) {
            for (var tierIdx = 0; tierIdx < ctx.trees[treeIdx].tiers.length; tierIdx++) {
                var mdk = ctx.trees[treeIdx].tiers[tierIdx].masteries;
                for (var masteryIdx = 0; masteryIdx < mdk.length; masteryIdx++) {

                    if (ctx.trees[treeIdx].treeTotal < (tierIdx * 4)) {
                        mdk[masteryIdx].isDisabled = true;
                    } else {
                        mdk[masteryIdx].isDisabled = false;
                    }

                    if (30 === ctx.buildTotal && 0 === mdk[masteryIdx].rank) {
                        mdk[masteryIdx].isDisabled = true;
                    }
                }
            }
        }

        return ctx;
    },

    setContext: function () {
        var season = $.fn.LeaguePress.findByCode("masteries", this.seasonCode);

        if (null == season) {
            throw {
                name: "MasteriesNotFound",
                message: "Masteries not found."
            };
        }

        if (null == this.context) {
            // deep clone
            this.context = $.extend(true, {}, season);
        }

        return season;
    }
};