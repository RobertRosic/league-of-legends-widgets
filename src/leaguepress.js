if ("undefined" === typeof window.resourcePath) {
    window.resourcePath = './src/';
}

function log(obj) {
    $.fn.LeaguePress.log(obj);
}

function loadResource(path, resourceName, dataType) {
    if ("undefined" === typeof dataType) {
        dataType = "json";
    }

    if ("undefined" === typeof $.fn.LeaguePress[resourceName]) {
        // http://api.jquery.com/jQuery.ajax/

        return $.ajax(path, {
            "async" : false,
            "dataType" : dataType
        })
            .done(function (data) {
                $.fn.LeaguePress[resourceName] = data;
            }).error(function (jqXHR, textStatus, errorThrown) {
                // http://api.jquery.com/jQuery.ajax/#jqXHR
                log(jqXHR.status + " : " + textStatus + " : " + errorThrown);
            });
    }
}
window.loadResource = loadResource;

function loadTemplate(fileName, templateName) {
    if ("LeaguePress" in window && fileName in window["LeaguePress"]) {
        return window["LeaguePress"][fileName];
    }

    loadResource(resourcePath + 'templates/' + fileName, templateName, 'html');
    return Handlebars.compile($.fn.LeaguePress[templateName]);
}
window.loadTemplate = loadTemplate;

// bootstrap the league of legends data
$.fn.LeaguePress = $.fn.LeaguePress || {};

$.fn.LeaguePress.resources =
    $.fn.LeaguePress.resources ||
    {
        'champions' : null,
        'summonerspells' : null,
        'runes' : null,
        'items' : null,
        'masteries' : null
    };

$.fn.LeaguePress.debug = $.fn.LeaguePress.debug || false;

$.fn.LeaguePress.log = function (o) {
    if ($.fn.LeaguePress.debug) {
        console.log(o);
    }
};

$.fn.LeaguePress.log("Logging enabled.");

/**
 * Find data in a specific LeaguePress resource.
 *
 * @param resourceName
 * @param code
 * @returns {*}
 */
$.fn.LeaguePress.findByCode = function (resourceName, code) {
    for (var index in $.fn.LeaguePress[resourceName]) {
        if (code === $.fn.LeaguePress[resourceName][index].code) {
            return $.fn.LeaguePress[resourceName][index];
        }
    }

    return null;
};

$.fn.LeaguePress.promises = [];

for (var key in $.fn.LeaguePress.resources) {
    var ref = $.fn.LeaguePress.resources[key];

    if (null !== ref && ref.substr(0, 4) === "http") {
        $.fn.LeaguePress.promises.push(loadResource(ref, key, "jsonp"));
    } else {
        $.fn.LeaguePress.promises.push(loadResource(resourcePath + 'library/' + key + '.json', key));
    }
}

Handlebars.registerHelper('ifInnate', function (archetype, slots, options) {
    if (archetype === "innate") {
        return options.fn(slots);
    }

    return options.inverse({
        "slots" : slots,
        "ability" : this
    });
});

Handlebars.registerHelper('ifMoreThanOne', function (number, context, options) {
    if (number > 1) {
        return options.fn(context);
    }

    return "";
});

Handlebars.registerHelper('cssCode', function (code, options) {
    if (typeof(code) === "undefined" || null === code) {
        return "undefined";
    }

    return code.replace(/[\s_]+/g, '-');
});