/**
 * LeaguePress leagueTooltip jQuery UI Plugin
 */
$.widget("leaguepress.leagueTooltip", {
    options: {
        target: null,
        class: 'leaguepress-ui-tooltip',
        template: null,
        timeout: 2000,
        codeAttributeName: 'data-code',
        leaguePressResourceName: 'champions',
        timeoutHandler: null
    },

    _init: function () {
        var that = this;

        if (null === this.options.template) {
            throw {
                name: 'TemplateNotFound',
                message: "Template required, none found."
            };
        }

        $(that.element)
            .addClass(that.options.class);

        $(this.options.target).on('mouseover', '[' + this.options.codeAttributeName + ']', function () {
            var position = $(this).offset();
            that.render.call(that, this, position);
        });
    },

    _destroy: function () {
        $(this.options.target).unbind('mouseover');
    },

    render: function (element, position) {
        if (typeof this.options.template === 'function') {
            var code = $(element).attr(this.options.codeAttributeName);
            var data = $.fn.LeaguePress.findByCode(this.options.leaguePressResourceName, code);
            if (data) {
                var content = this.options.template(data);
                this.show(content, position);
            }
        }
    },

    show: function (content, position) {
        var that = this;

        $(this.element)
            .html(content)
            .css({
                "opacity": '0.8',
                "top": (position.top + 40) + "px",
                "left": (position.left + 40) + "px"
            })
            .fadeIn();

        // reset the previous timer
        clearTimeout(this.options.timeoutHandler);
        this.options.timeoutHandler = setTimeout(function () {
            that.hide();
        }, that.options.timeout);
    },

    hide: function () {
        $(this.element).hide();
    }
});
// Could also use http://qtip2.com/

/**
 * LeaguePress leagueSelect jQuery UI Plugin
 */
$.widget("leaguepress.leagueSelect", {
    options: {
        "template": null,
        "selected": null,
        "closeOnSelect": true,
        "top": 50,
        "left": 50,
        "leaguePressResourceName": "champions",
        "singularName": 'champion',
        "pluralName": 'champions',
        "title": "Champions",
        "icon": {height: "32px", width: "32px"}
    },

    _init: function () {
        var that = this;

        if (null === this.options.template) {
            this.options.template = window.loadTemplate('league_select.handlebars', "league_select.handlebars");
        }

        $(that.element)
            .draggable()
            .css({
                "position": "absolute",
                "top": that.options.top + "px",
                "left": that.options.left + "px"
            });

        $(that.element).on("onSelect", that.onSelect);
        that.options.index = 0;
        that._render();
        that.hide();
    },

    show: function () {
        $(this.element).show();
    },

    hide: function () {
        $(this.element).hide();
    },

    /**
     * @param event
     * @param spell
     * @returns {boolean}
     */
    onSelect: function (event, spell) {
        return true;
    },

    _bindEvents: function () {
        var that = this;
        var element = $(this.element);
        var op = that.options;

        $("input[name='filter']", element).on('keyup', function () {
            var filter = $(this).val();
            var regex = new RegExp(filter, 'gi');

            $("div[data-" + op.singularName + "-code]", element).each(function (index, element) {
                var value = $(element).attr("data-" + op.singularName + "-code");
                var isMatch = regex.test(value);
                if (!isMatch) {
                    $(element).closest('li').hide();
                } else {
                    $(element).closest('li').show();
                }
            });
        });

        $("span.glyphicon-remove", element).click(function () {
            that.hide.call(that);
        });

        $("ul", element).on('click', 'li', function () {
            var codeElement = $("[data-" + op.singularName + "-code]", this).first();
            op.selected = codeElement.attr("data-" + op.singularName + "-code");

            $(that.element).trigger("onSelect", op.selected);
            if (op.closeOnSelect) {
                that.hide.call(that);
            }

            return false;
        });
    },

    _unbindEvents: function () {
        var element = $(this.element);

        $("input[name='filter']", element).unbind('keyup');
        $("span.glyphicon-remove", element).unbind('click');
        $("ul", element).unbind('click');
    },

    _render: function () {
        var that = this;
        var ele = $(this.element);
        var op = that.options;

        that._unbindEvents();

        var data = {
            title: op.title,
            singularName: op.singularName,
            pluralName: op.pluralName,
            list: $.fn.LeaguePress[op.leaguePressResourceName]
        };

        // render the template
        ele.html(op.template(data));

        $(".ui-list li div", that.element).css({
            "height": op.icon.height,
            "width": op.icon.width
        });

        that._bindEvents();
    }
});