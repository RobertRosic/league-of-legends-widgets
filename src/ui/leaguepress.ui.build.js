$.widget("leaguepress.leaguepressbuild", {
    options : {
        champion : null,
        championSelect : null,
        runes : null,
        runesSelect : null,
        items : null,
        itemsSelect : null,
        summonerspells : null,
        summonerspellsSelect : null,
        masteries : null
    },

    _init : function () {
        var that = this;
        var op = this.options;

        /**
         * CHAMPION
         */
        op.championSelect = $('<div/>').leagueSelect({
            closeOnSelect : false,
            "icon" : {height : "32px", width : "32px"}
        }).on('onSelect',function (event, championCode) {
                op.champion.champion('setChampion', championCode);
            }).appendTo(that.element);

        op.champion = $('<div/>').champion({
            template : loadTemplate('champion.handlebars', "champion.handlebars"),
            champion : "ashe"
        }).on("selectChampion",function () {
                op.championSelect.leagueSelect("show");
            }).appendTo(that.element);

        /**
         * RUNES
         */
        op.runes = $('<div/>').runesbuild({
            template : loadTemplate('runes_build.handlebars', "runes_build.handlebars")
        }).on("showRunes",function () {
                op.runesSelect.leagueSelect('show');
            }).appendTo(that.element);

        op.runesSelect = $('<div/>').leagueSelect({
            closeOnSelect : false,
            template : loadTemplate('runes_select.handlebars', "runes_select.handlebars"),
            "leaguePressResourceName" : "runes",
            "singularName" : 'rune',
            "pluralName" : 'runes',
            "title" : "Runes",
            "icon" : {height : "48px", width : "48px"}
        }).on('onSelect',
            function (event, rune) {
                op.runes.runesbuild('addRune', rune);
            }).appendTo(that.element);

        /**
         * ITEMS
         */
        $.fn.LeaguePress.findByCode("items", "health_potion").amountPerSlot = 5;
        $.fn.LeaguePress.findByCode("items", "mana_potion").amountPerSlot = 5;
        $.fn.LeaguePress.findByCode("items", "sight_ward").amountPerSlot = 5;
        $.fn.LeaguePress.findByCode("items", "vision_ward").amountPerSlot = 5;

        op.itemsSelect = $('<div/>').leagueSelect({
            closeOnSelect : false,
            "leaguePressResourceName" : "items",
            "singularName" : 'item',
            "pluralName" : 'items',
            "title" : "Items",
            "icon" : {height : "48px", width : "48px"}
        }).on('onSelect',
            function (event, item) {
                op.items.itemsbuild('setItem', item);
            }).appendTo(that.element);

        op.items = $('<div/>').itemsbuild({
            template : loadTemplate('items_build.handlebars', "items_build.handlebars")
        }).itemsbuild().on("showItems",function () {
                op.itemsSelect.leagueSelect("show");
            }).appendTo(that.element);

        /**
         * SUMMONER SPELLS
         */
        op.summonerspellsSelect = $('<div/>').leagueSelect({
            closeOnSelect : true,
            "leaguePressResourceName" : "summonerspells",
            "singularName" : 'summonerspell',
            "pluralName" : 'summonerspells',
            "title" : "Spells",
            "icon" : {height : "48px", width : "48px"}
        }).on('onSelect',
            function (event, spell) {
                // ?
            }).appendTo(that.element);

        op.summonerspells = $('<div/>').championSpells({
            template : loadTemplate('champion_summonerspells.handlebars', "champion_summonerspells.handlebars"),
            selector : op.summonerspellsSelect
        }).appendTo(that.element);

        /**
         * MASTERIES
         */
        op.masteries = $('<div/>').masteriesbuild({
            template : loadTemplate('masteries_build.handlebars', "masteries_build.handlebars")
        }).appendTo(that.element);

        op.champion.css({"margin-bottom" : "20px"});
        op.summonerspells.css({"margin-bottom" : "20px"});
        op.items.css({"margin-bottom" : "20px"});
        op.runes.css({"margin-bottom" : "20px"});
        op.masteries.css({"margin-bottom" : "20px"});
    },

    save : function () {
        var op = this.options;

        var dt = {
            "champion": op.champion.champion("save"),
            "runes": op.runes.runesbuild("save"),
            "items": op.items.itemsbuild("save"),
            "summonerspells": op.summonerspells.championSpells("save"),
            "masteries": op.masteries.masteriesbuild("save")
        };

        return dt;
    },

    load : function (data) {
        for (var prop in data) {
           this._loadPart(prop, data);
        }
    },

    _loadPart : function(part, data) {
        var op = this.options;

        if (part) {
            switch (part) {
                case "champion":
                    op.champion.champion("load", data.champion);
                    break;
                case "runes":
                    op.runes.runesbuild("load", data.runes);
                    break;
                case "items":
                    op.items.itemsbuild("load", data.items);
                    break;
                case "summonerspells":
                    op.summonerspells.championSpells("load", data.summonerspells);
                    break;
                case "masteries":
                    op.masteries.masteriesbuild("load", data.masteries);
                    break;
            }
        }
    },

    clear : function () {
        var op = this.options;

        op.champion.champion("clear");
        op.runes.runesbuild("clear");
        op.items.itemsbuild("clear");
        op.summonerspells.championSpells("clear");
        op.masteries.masteriesbuild("clear");
    }
});