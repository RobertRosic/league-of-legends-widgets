$.widget("leaguepress.runesbuild", {
    options: {
        template: null,
        runesbuild: null
    },

    save: function () {
        return this.options.runesbuild.save();
    },

    load: function(data) {
        this.options.runesbuild = new RunesBuild();
        this.options.runesbuild.load(data);
        this._render();
    },

    clear: function() {
        this.options.runesbuild = new RunesBuild();
        this._render();
    },

    _init: function () {
        this.options.runesbuild = new RunesBuild();

        if (null === this.options.template) {
            this.options.template = window.loadTemplate('runes_build.handlebars', "runes_build.handlebars");
        }

        this._render();
    },

    _bindEvents: function () {
        var that = this;

        $(".runes-show", that.element).on("click.runesbuild", function () {
            $(that.element).trigger("showRunes");

            return false;
        });

        $("li", that.element).on("click.runesbuild", function () {
            var runeCode = $("[data-rune-code]", this).attr("data-rune-code");
            that.options.runesbuild.addRune(runeCode);
            that._render();
        });

        $("li", that.element).on("contextmenu.runesbuild", function () {
            var runeCode = $("[data-rune-code]", this).attr("data-rune-code");
            that.options.runesbuild.removeRune(runeCode);
            that._render();
            return false;
        });

        $("li", that.element).on("click.runesbuild", ".remove-rune", function () {
            var runeCode = $(this).closest("[data-rune-code]").attr("data-rune-code");
            that.options.runesbuild.removeRune(runeCode);
            that._render();
            return false;
        });

        $("li", that.element).on("click.runesbuild", ".add-rune", function () {
            var runeCode = $(this).closest("[data-rune-code]").attr("data-rune-code");
            that.options.runesbuild.addRune(runeCode);
            that._render();
            return false;
        });
    },

    _unbindEvents: function () {
        var that = this;
        $('li', that.element).unbind(".runesbuild");
        $(".runes-show", that.element).unbind(".runesbuild");
    },

    _render: function () {
        var that = this;
        var ele = $(this.element);
        that._unbindEvents();

        // render the template
        ele.html(this.options.template({
            "runesbuild": this.options.runesbuild
        }));

        that._bindEvents();
    },

    addRune: function (runeCode) {
        this.options.runesbuild.addRune(runeCode);
        this._render();
    }
});
