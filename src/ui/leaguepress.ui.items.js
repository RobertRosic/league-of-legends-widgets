$.widget("leaguepress.itemsbuild", {
    options : {
        template : null,
        infoTemplate : null,
        itemsbuild : null,
        itemIndex : 0,
        itemSetIndex : 0
    },

    save : function () {
        return this.options.itemsbuild.save();
    },

    clear : function() {
        this.options.itemsbuild = new ItemsBuild();
        this._render();
    },

    load : function (data) {
        var op = this.options;

        if (data) {
            op.itemsbuild = new ItemsBuild();
            op.itemsbuild.removeItemSet(0);
            $.each(data, function (itemSetIndex, itemSet) {
                var is = op.itemsbuild.addItemSet(itemSet.items);
                is.title = itemSet.title;
            });
        }

        this._render();
    },

    _init : function () {
        if (null === this.options.template) {
            if ("LeaguePress" in window) {
                this.options.template = window["LeaguePress"]['items_build.handlebars'];
            }
        }

        this.options.itemsbuild = new ItemsBuild();
        this.options.itemsbuild.addItem("boots_of_speed");
        this.options.itemsbuild.addItem("health_potion");
        this.options.itemsbuild.addItemAtIndex("health_potion", 0, 1);
        this.options.itemsbuild.addItemAtIndex("health_potion", 0, 1);
        this._render();
    },

    _bindEvents : function () {
        var that = this;
        var ele = $(that.element);

        $(".items-show", that.element).on("click.itemsbuild", function () {
            $(ele).trigger("showItems");

            return false;
        });

        $(".itemset-add", that.element).on("click.itemsbuild", function () {
            that.options.itemsbuild.addItemSet();
            that._render();

            return false;
        });

        $(".itemset-remove", that.element).on("click.itemsbuild", function () {
            var ix = $(this).parent("div[data-itemset-index]").attr("data-itemset-index");
            that.options.itemsbuild.removeItemSet(ix);
            that._render();

            return false;
        });

        $(".title input", that.element).on("keyup.itemsbuild", function () {
            var title = $(this).val();
            var ixs = $(this).parents("div[data-itemset-index]").first().attr("data-itemset-index");
            that.options.itemsbuild.setTitle(title, ixs);
        });

        $(".item-remove", that.element).on("click.itemsbuild", function () {
            var ix = $("div[data-item-index]", $(this).parent("")).attr("data-item-index");
            var ixs = $(this).parents("div[data-itemset-index]").first().attr("data-itemset-index");
            that.options.itemsbuild.removeItemAtIndex(ixs, ix);
            that._render();

            return false;
        });

        $(".item-add", that.element).on("click.itemsbuild", function () {
            var ix = $("div[data-item-index]", $(this).parent("")).attr("data-item-index");
            var ixs = $(this).parents("div[data-itemset-index]").first().attr("data-itemset-index");
            var itemCode = $("div[data-item-code]", $(this).parent("")).attr("data-item-code");
            that.options.itemsbuild.addItemAtIndex(itemCode, ixs, ix);
            that._render();

            return false;
        });

        $("div[data-item-index]", that.element).on("click.itemsbuild", function () {
            that.options.itemIndex = $(this).attr("data-item-index");
            that.options.itemSetIndex =
                $(this).parents("div[data-itemset-index]").first().attr("data-itemset-index");
            that._render();

            return false;
        });

        // mouseover info
        $("div[data-item-code]", ele).on("mouseover.itemsbuild", function (event) {
            var code = $(this).attr("data-item-code");
            var item = $.fn.LeaguePress.findByCode("items", code);

            if (item) {
                if (that.options.infoTemplate) {
                    var position = $(this).offset();
                    $(".item-info").on("click.itemsbuild", function () {
                        $(this).hide();
                    });

                    $(".item-info").html(that.options.infoTemplate({
                            "item" : item
                        })).css({
                            "top" : (position.top + 40) + "px",
                            "left" : (position.left + 40) + "px",
                            "z-index" : "200",
                            "opacity" : "0.8"
                        }).fadeIn();

                    // reset the previous timer
                    clearTimeout(itemInfoHandler);
                    itemInfoHandler = setTimeout(function () {
                        $(".item-info").hide();
                    }, 2500);
                }
            }
        });
    },

    _unbindEvents : function () {
        var that = this;
        $('div', that.element).unbind(".itemsbuild");
    },

    _render : function () {
        var that = this;
        var ele = $(this.element);
        that._unbindEvents();

        // render the template
        ele.html(this.options.template({
            itemsbuild : this.options.itemsbuild
        }));

        var ixs = that.options.itemSetIndex;
        var ix = that.options.itemIndex;

        $("div[data-itemset-index=" + ixs + "] div[data-item-index=" + ix + "]", ele)
            .parents("li")
            .first()
            .addClass("selected");

        that._bindEvents();
    },

    setItem : function (itemCode) {
        var that = this;
        that.options.itemsbuild.addItemAtIndex(itemCode,
            that.options.itemSetIndex, that.options.itemIndex);

        that.options.itemIndex++;
        that.options.itemIndex = ItemSet.prototype.ensureValidIndex(that.options.itemIndex);

        that._render();
    }
});
