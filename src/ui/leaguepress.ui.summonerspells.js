/**
 * LeaguePress Champion SummonerSpells jQuery UI Plugin
 */
$.widget("leaguepress.championSpells", {
    options: {
        template: null,
        spells: null,
        selectedIndex: 0,
        selector: null
    },

    save : function() {
        return this.options.spells.save();
    },

    load : function(data) {
        this.options.spells = new ChampionSpells();
        this.options.spells.load(data);
        this._render();
    },

    clear : function() {
        this.options.spells = new ChampionSpells();
        this.options.spells.setSpell(0, "flash");
        this.options.spells.setSpell(1, "ignite");
        this._render();
    },

    _init: function () {
        var that = this;

        if (null === this.options.template) {
            if ("LeaguePress" in window) {
                this.options.template = window["LeaguePress"]['champion_summonerspells.handlebars'];
            }
        }

        that.options.spells = new ChampionSpells();
        that.options.spells.setSpell(0, "flash");
        that.options.spells.setSpell(1, "ignite");

        that._render();
    },

    _render: function () {
        var that = this;
        var spellsSelect = that.options.selector;

        spellsSelect.unbind("onSelect");
        $("div[data-summonerspell-index]", that.element).unbind("click");

        $(that.element).html(that.options.template({
            "summonerspells": that.options.spells.getSpells()
        }));

        $("div[data-summonerspell-index]", that.element).removeClass("selected");
        $("div[data-summonerspell-index='" + that.options.selectedIndex + "']", that.element).addClass("selected");

        spellsSelect.on("onSelect", function (event, spell) {
            that.options.spells.setSpell(that.options.selectedIndex, spell);
            that._render();
        });

        $("div[data-summonerspell-index]", that.element).click(function () {
            that.options.selectedIndex = parseInt($(this).attr("data-summonerspell-index"), 10);
            spellsSelect.leagueSelect("show");
            that._render();
        });
    },

    getSpells: function () {
        return this.options.spells.getSpells();
    }
});
