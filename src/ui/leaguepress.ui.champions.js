$.widget("leaguepress.champion", {
    options: {
        template: null,
        champion: null
    },

    save: function () {
        return this.options.champion.save();
    },

    load: function(data) {
        var op = this.options;
        var championData = $.fn.LeaguePress.findByCode("champions", data.code);
        if (championData) {
            op.champion = new Champion();
            op.champion.setData(championData);
            $.each(data.abilities, function(index, obj){
                op.champion.build.abilities[obj.code] = obj.levels;
            });
            op.champion.build.level = data.level;
            this._render();
        }
    },

    clear: function() {
        this.setChampion("teemo");
    },

    _init: function () {
        if (null === this.options.template) {
            if ("LeaguePress" in window) {
                this.options.template = window["LeaguePress"]['champion.handlebars'];
            }
        }

        if (null !== this.options.champion) {
            this.setChampion(this.options.champion);
        }
    },

    setChampion: function (championCode) {
        var that = this;
        var champion = $.fn.LeaguePress.findByCode("champions", championCode);
        that.options.champion = new Champion();
        that.options.champion.setData(champion);
        that._render();
    },

    _bindEvents: function () {
        var that = this;

        $("td[data-ability]", that.element).on("click.champion", function () {
            if ($(this).is('.ability-selected')) {
                $(this).removeClass('ability-selected');
                that.options.champion.removeBuildAbility($(this).attr('data-ability'), $(this).attr('data-slot'));
            } else {
                that.options.champion.addBuildAbility($(this).attr('data-ability'), $(this).attr('data-slot'));
            }
            that._render();
        });

        $('[data-champion-code]', that.element).on("click.champion", function () {
            $(that.element).trigger("selectChampion");
        });
    },

    _unbindEvents: function () {
        var that = this;
        $('td', that.element).unbind(".champion");
        $('div', that.element).unbind(".champion");
    },

    _render: function () {
        var that = this;
        var ele = $(that.element);

        that._unbindEvents();

        ele.html(this.options.template({
            "champion": that.options.champion,
            "slots": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18],
            "debug": JSON.stringify(that.options.champion)
        }));

        // loop build abilities
        $.each(that.options.champion.build.abilities, function (abilityCode, selectedList) {
            $.each(selectedList, function (i, slotNumber) {
                $(".champion-ability[data-ability='" + abilityCode + "'][data-slot='" + slotNumber + "']")
                    .addClass("ability-selected");
            });
        });

        that._bindEvents();
    }
});