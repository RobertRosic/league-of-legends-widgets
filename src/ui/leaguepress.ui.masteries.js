$.widget("leaguepress.masteriesbuild", {
    options : {
        masteriesbuild : null,
        template : null
    },

    save : function () {
        return this.options.masteriesbuild.save();
    },

    load : function (data) {
        var op = this.options;

        if (data) {
            var master = new MasteriesBuild();

            master.build = {};
            $.each(data, function(index, object){
                master.build[object.code] = object.rank;
            });

            if (master.validate()) {
                op.masteriesbuild = master;
                this._render();
            }
        }
    },

    clear : function() {
        this.options.masteriesbuild = new MasteriesBuild();
        this._render();
    },

    _init : function () {
        this.options.masteriesbuild = new MasteriesBuild();

        if (null === this.options.template) {
            if ("LeaguePress" in window) {
                this.options.template = window["LeaguePress"]['masteries_build.handlebars'];
            }
        }

        this._render();
    },

    _bindEvents : function () {
        var that = this;
        var ele = $(that.element);

        $("[data-mastery-code]", ele).on("click.masteriesbuild", function () {
            var mastery = $(this).attr("data-mastery-code");
            that.options.masteriesbuild.addMastery(mastery);
            that._render();
        });

        $("[data-mastery-code]", ele).on("contextmenu.masteriesbuild", function () {
            var mastery = $(this).attr("data-mastery-code");
            that.options.masteriesbuild.removeMastery(mastery);
            that._render();
            return false;
        });
    },

    _unbindEvents : function () {
        var that = this;
        $('div', that.element).unbind(".masteriesbuild");
    },

    _render : function () {
        var that = this;
        var ele = $(that.element);
        that._unbindEvents();

        // render the template
        ele.html(this.options.template({
            masteriesbuild : this.options.masteriesbuild.getContext()
        }));

        that._bindEvents();
    }
});