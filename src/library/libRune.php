<?php global $libRune; $libRune = array (
  'greater_glyph_of_ability_power' => 
  array (
    'iconCode' => 'b_3_3',
    'shortName' => 'Greater Glyph of Ability Power',
    'code' => 'greater_glyph_of_ability_power',
  ),
  'greater_glyph_of_armor' => 
  array (
    'iconCode' => 'b_1_3',
    'shortName' => 'Greater Glyph of Armor',
    'code' => 'greater_glyph_of_armor',
  ),
  'greater_glyph_of_attack_damage' => 
  array (
    'iconCode' => 'b_1_3',
    'shortName' => 'Greater Glyph of Attack Damage',
    'code' => 'greater_glyph_of_attack_damage',
  ),
  'greater_glyph_of_attack_speed' => 
  array (
    'iconCode' => 'b_3_3',
    'shortName' => 'Greater Glyph of Attack Speed',
    'code' => 'greater_glyph_of_attack_speed',
  ),
  'greater_glyph_of_cooldown_reduction' => 
  array (
    'iconCode' => 'b_1_3',
    'shortName' => 'Greater Glyph of Cooldown Reduction',
    'code' => 'greater_glyph_of_cooldown_reduction',
  ),
  'greater_glyph_of_critical_chance' => 
  array (
    'iconCode' => 'b_3_3',
    'shortName' => 'Greater Glyph of Critical Chance',
    'code' => 'greater_glyph_of_critical_chance',
  ),
  'greater_glyph_of_critical_damage' => 
  array (
    'iconCode' => 'b_1_3',
    'shortName' => 'Greater Glyph of Critical Damage',
    'code' => 'greater_glyph_of_critical_damage',
  ),
  'greater_glyph_of_energy' => 
  array (
    'iconCode' => 'b_3_3',
    'shortName' => 'Greater Glyph of Energy',
    'code' => 'greater_glyph_of_energy',
  ),
  'greater_glyph_of_health' => 
  array (
    'iconCode' => 'b_3_3',
    'shortName' => 'Greater Glyph of Health',
    'code' => 'greater_glyph_of_health',
  ),
  'greater_glyph_of_health_regeneration' => 
  array (
    'iconCode' => 'b_1_3',
    'shortName' => 'Greater Glyph of Health Regeneration',
    'code' => 'greater_glyph_of_health_regeneration',
  ),
  'greater_glyph_of_magic_penetration' => 
  array (
    'iconCode' => 'b_1_3',
    'shortName' => 'Greater Glyph of Magic Penetration',
    'code' => 'greater_glyph_of_magic_penetration',
  ),
  'greater_glyph_of_magic_resist' => 
  array (
    'iconCode' => 'b_3_3',
    'shortName' => 'Greater Glyph of Magic Resist',
    'code' => 'greater_glyph_of_magic_resist',
  ),
  'greater_glyph_of_mana' => 
  array (
    'iconCode' => 'b_1_3',
    'shortName' => 'Greater Glyph of Mana',
    'code' => 'greater_glyph_of_mana',
  ),
  'greater_glyph_of_mana_regeneration' => 
  array (
    'iconCode' => 'b_3_3',
    'shortName' => 'Greater Glyph of Mana Regeneration',
    'code' => 'greater_glyph_of_mana_regeneration',
  ),
  'greater_glyph_of_scaling_ability_power' => 
  array (
    'iconCode' => 'b_4_3',
    'shortName' => 'Greater Glyph of Scaling Ability Power',
    'code' => 'greater_glyph_of_scaling_ability_power',
  ),
  'greater_glyph_of_scaling_attack_damage' => 
  array (
    'iconCode' => 'b_2_3',
    'shortName' => 'Greater Glyph of Scaling Attack Damage',
    'code' => 'greater_glyph_of_scaling_attack_damage',
  ),
  'greater_glyph_of_scaling_cooldown_reduction' => 
  array (
    'iconCode' => 'b_2_3',
    'shortName' => 'Greater Glyph of Scaling Cooldown Reduction',
    'code' => 'greater_glyph_of_scaling_cooldown_reduction',
  ),
  'greater_glyph_of_scaling_energy' => 
  array (
    'iconCode' => 'b_2_3',
    'shortName' => 'Greater Glyph of Scaling Energy',
    'code' => 'greater_glyph_of_scaling_energy',
  ),
  'greater_glyph_of_scaling_health' => 
  array (
    'iconCode' => 'b_4_3',
    'shortName' => 'Greater Glyph of Scaling Health',
    'code' => 'greater_glyph_of_scaling_health',
  ),
  'greater_glyph_of_scaling_magic_resist' => 
  array (
    'iconCode' => 'b_4_3',
    'shortName' => 'Greater Glyph of Scaling Magic Resist',
    'code' => 'greater_glyph_of_scaling_magic_resist',
  ),
  'greater_glyph_of_scaling_mana' => 
  array (
    'iconCode' => 'b_2_3',
    'shortName' => 'Greater Glyph of Scaling Mana',
    'code' => 'greater_glyph_of_scaling_mana',
  ),
  'greater_glyph_of_scaling_mana_regeneration' => 
  array (
    'iconCode' => 'b_4_3',
    'shortName' => 'Greater Glyph of Scaling Mana Regeneration',
    'code' => 'greater_glyph_of_scaling_mana_regeneration',
  ),
  'greater_mark_of_ability_power' => 
  array (
    'iconCode' => 'r_3_3',
    'shortName' => 'Greater Mark of Ability Power',
    'code' => 'greater_mark_of_ability_power',
  ),
  'greater_mark_of_armor' => 
  array (
    'iconCode' => 'r_1_3',
    'shortName' => 'Greater Mark of Armor',
    'code' => 'greater_mark_of_armor',
  ),
  'greater_mark_of_armor_penetration' => 
  array (
    'iconCode' => 'r_1_3',
    'shortName' => 'Greater Mark of Armor Penetration',
    'code' => 'greater_mark_of_armor_penetration',
  ),
  'greater_mark_of_attack_damage' => 
  array (
    'iconCode' => 'r_1_3',
    'shortName' => 'Greater Mark of Attack Damage',
    'code' => 'greater_mark_of_attack_damage',
  ),
  'greater_mark_of_attack_speed' => 
  array (
    'iconCode' => 'r_3_3',
    'shortName' => 'Greater Mark of Attack Speed',
    'code' => 'greater_mark_of_attack_speed',
  ),
  'greater_mark_of_cooldown_reduction' => 
  array (
    'iconCode' => 'r_1_3',
    'shortName' => 'Greater Mark of Cooldown Reduction',
    'code' => 'greater_mark_of_cooldown_reduction',
  ),
  'greater_mark_of_critical_chance' => 
  array (
    'iconCode' => 'r_3_3',
    'shortName' => 'Greater Mark of Critical Chance',
    'code' => 'greater_mark_of_critical_chance',
  ),
  'greater_mark_of_critical_damage' => 
  array (
    'iconCode' => 'r_1_3',
    'shortName' => 'Greater Mark of Critical Damage',
    'code' => 'greater_mark_of_critical_damage',
  ),
  'greater_mark_of_health' => 
  array (
    'iconCode' => 'r_3_3',
    'shortName' => 'Greater Mark of Health',
    'code' => 'greater_mark_of_health',
  ),
  'greater_mark_of_hybrid_penetration' => 
  array (
    'iconCode' => 'r_1_3',
    'shortName' => 'Greater Mark of Hybrid Penetration',
    'code' => 'greater_mark_of_hybrid_penetration',
  ),
  'greater_mark_of_magic_penetration' => 
  array (
    'iconCode' => 'r_1_3',
    'shortName' => 'Greater Mark of Magic Penetration',
    'code' => 'greater_mark_of_magic_penetration',
  ),
  'greater_mark_of_magic_resist' => 
  array (
    'iconCode' => 'r_3_3',
    'shortName' => 'Greater Mark of Magic Resist',
    'code' => 'greater_mark_of_magic_resist',
  ),
  'greater_mark_of_mana' => 
  array (
    'iconCode' => 'r_1_3',
    'shortName' => 'Greater Mark of Mana',
    'code' => 'greater_mark_of_mana',
  ),
  'greater_mark_of_mana_regeneration' => 
  array (
    'iconCode' => 'r_3_3',
    'shortName' => 'Greater Mark of Mana Regeneration',
    'code' => 'greater_mark_of_mana_regeneration',
  ),
  'greater_mark_of_scaling_ability_power' => 
  array (
    'iconCode' => 'r_4_3',
    'shortName' => 'Greater Mark of Scaling Ability Power',
    'code' => 'greater_mark_of_scaling_ability_power',
  ),
  'greater_mark_of_scaling_attack_damage' => 
  array (
    'iconCode' => 'r_2_3',
    'shortName' => 'Greater Mark of Scaling Attack Damage',
    'code' => 'greater_mark_of_scaling_attack_damage',
  ),
  'greater_mark_of_scaling_health' => 
  array (
    'iconCode' => 'r_4_3',
    'shortName' => 'Greater Mark of Scaling Health',
    'code' => 'greater_mark_of_scaling_health',
  ),
  'greater_mark_of_scaling_magic_resist' => 
  array (
    'iconCode' => 'r_4_3',
    'shortName' => 'Greater Mark of Scaling Magic Resist',
    'code' => 'greater_mark_of_scaling_magic_resist',
  ),
  'greater_mark_of_scaling_mana' => 
  array (
    'iconCode' => 'r_2_3',
    'shortName' => 'Greater Mark of Scaling Mana',
    'code' => 'greater_mark_of_scaling_mana',
  ),
  'greater_quintessence_of_ability_power' => 
  array (
    'iconCode' => 'bl_3_3',
    'shortName' => 'Greater Quintessence of Ability Power',
    'code' => 'greater_quintessence_of_ability_power',
  ),
  'greater_quintessence_of_armor_penetration' => 
  array (
    'iconCode' => 'bl_1_3',
    'shortName' => 'Greater Quintessence of Armor Penetration',
    'code' => 'greater_quintessence_of_armor_penetration',
  ),
  'greater_quintessence_of_attack_damage' => 
  array (
    'iconCode' => 'bl_1_3',
    'shortName' => 'Greater Quintessence of Attack Damage',
    'code' => 'greater_quintessence_of_attack_damage',
  ),
  'greater_quintessence_of_attack_speed' => 
  array (
    'iconCode' => 'bl_3_3',
    'shortName' => 'Greater Quintessence of Attack Speed',
    'code' => 'greater_quintessence_of_attack_speed',
  ),
  'greater_quintessence_of_cooldown_reduction' => 
  array (
    'iconCode' => 'bl_1_3',
    'shortName' => 'Greater Quintessence of Cooldown Reduction',
    'code' => 'greater_quintessence_of_cooldown_reduction',
  ),
  'greater_quintessence_of_critical_chance' => 
  array (
    'iconCode' => 'bl_3_3',
    'shortName' => 'Greater Quintessence of Critical Chance',
    'code' => 'greater_quintessence_of_critical_chance',
  ),
  'greater_quintessence_of_critical_damage' => 
  array (
    'iconCode' => 'bl_1_3',
    'shortName' => 'Greater Quintessence of Critical Damage',
    'code' => 'greater_quintessence_of_critical_damage',
  ),
  'greater_quintessence_of_energy' => 
  array (
    'iconCode' => 'bl_3_3',
    'shortName' => 'Greater Quintessence of Energy',
    'code' => 'greater_quintessence_of_energy',
  ),
  'greater_quintessence_of_energy_regeneration' => 
  array (
    'iconCode' => 'bl_2_3',
    'shortName' => 'Greater Quintessence of Energy Regeneration',
    'code' => 'greater_quintessence_of_energy_regeneration',
  ),
  'greater_quintessence_of_experience' => 
  array (
    'iconCode' => 'bl_2_3',
    'shortName' => 'Greater Quintessence of Experience',
    'code' => 'greater_quintessence_of_experience',
  ),
  'greater_quintessence_of_gold' => 
  array (
    'iconCode' => 'bl_4_3',
    'shortName' => 'Greater Quintessence of Gold',
    'code' => 'greater_quintessence_of_gold',
  ),
  'greater_quintessence_of_health' => 
  array (
    'iconCode' => 'bl_3_3',
    'shortName' => 'Greater Quintessence of Health',
    'code' => 'greater_quintessence_of_health',
  ),
  'greater_quintessence_of_health_regeneration' => 
  array (
    'iconCode' => 'bl_1_3',
    'shortName' => 'Greater Quintessence of Health Regeneration',
    'code' => 'greater_quintessence_of_health_regeneration',
  ),
  'greater_quintessence_of_hybrid_penetration' => 
  array (
    'iconCode' => 'bl_4_3',
    'shortName' => 'Greater Quintessence of Hybrid Penetration',
    'code' => 'greater_quintessence_of_hybrid_penetration',
  ),
  'greater_quintessence_of_life_steal' => 
  array (
    'iconCode' => 'bl_1_3',
    'shortName' => 'Greater Quintessence of Life Steal',
    'code' => 'greater_quintessence_of_life_steal',
  ),
  'greater_quintessence_of_magic_penetration' => 
  array (
    'iconCode' => 'bl_1_3',
    'shortName' => 'Greater Quintessence of Magic Penetration',
    'code' => 'greater_quintessence_of_magic_penetration',
  ),
  'greater_quintessence_of_magic_resist' => 
  array (
    'iconCode' => 'bl_3_3',
    'shortName' => 'Greater Quintessence of Magic Resist',
    'code' => 'greater_quintessence_of_magic_resist',
  ),
  'greater_quintessence_of_mana' => 
  array (
    'iconCode' => 'bl_1_3',
    'shortName' => 'Greater Quintessence of Mana',
    'code' => 'greater_quintessence_of_mana',
  ),
  'greater_quintessence_of_mana_regeneration' => 
  array (
    'iconCode' => 'bl_3_3',
    'shortName' => 'Greater Quintessence of Mana Regeneration',
    'code' => 'greater_quintessence_of_mana_regeneration',
  ),
  'greater_quintessence_of_movement_speed' => 
  array (
    'iconCode' => 'bl_3_3',
    'shortName' => 'Greater Quintessence of Movement Speed',
    'code' => 'greater_quintessence_of_movement_speed',
  ),
  'greater_quintessence_of_percent_health' => 
  array (
    'iconCode' => 'bl_3_3',
    'shortName' => 'Greater Quintessence of Percent Health',
    'code' => 'greater_quintessence_of_percent_health',
  ),
  'greater_quintessence_of_revival' => 
  array (
    'iconCode' => 'bl_1_3',
    'shortName' => 'Greater Quintessence of Revival',
    'code' => 'greater_quintessence_of_revival',
  ),
  'greater_quintessence_of_scaling_ability_power' => 
  array (
    'iconCode' => 'bl_4_3',
    'shortName' => 'Greater Quintessence of Scaling Ability Power',
    'code' => 'greater_quintessence_of_scaling_ability_power',
  ),
  'greater_quintessence_of_scaling_armor' => 
  array (
    'iconCode' => 'bl_2_3',
    'shortName' => 'Greater Quintessence of Scaling Armor',
    'code' => 'greater_quintessence_of_scaling_armor',
  ),
  'greater_quintessence_of_scaling_attack_damage' => 
  array (
    'iconCode' => 'bl_2_3',
    'shortName' => 'Greater Quintessence of Scaling Attack Damage',
    'code' => 'greater_quintessence_of_scaling_attack_damage',
  ),
  'greater_quintessence_of_scaling_cooldown_reduction' => 
  array (
    'iconCode' => 'bl_2_3',
    'shortName' => 'Greater Quintessence of Scaling Cooldown Reduction',
    'code' => 'greater_quintessence_of_scaling_cooldown_reduction',
  ),
  'greater_quintessence_of_scaling_health' => 
  array (
    'iconCode' => 'bl_4_3',
    'shortName' => 'Greater Quintessence of Scaling Health',
    'code' => 'greater_quintessence_of_scaling_health',
  ),
  'greater_quintessence_of_scaling_health_regeneration' => 
  array (
    'iconCode' => 'bl_2_3',
    'shortName' => 'Greater Quintessence of Scaling Health Regeneration',
    'code' => 'greater_quintessence_of_scaling_health_regeneration',
  ),
  'greater_quintessence_of_scaling_magic_resist' => 
  array (
    'iconCode' => 'bl_4_3',
    'shortName' => 'Greater Quintessence of Scaling Magic Resist',
    'code' => 'greater_quintessence_of_scaling_magic_resist',
  ),
  'greater_quintessence_of_scaling_mana' => 
  array (
    'iconCode' => 'bl_2_3',
    'shortName' => 'Greater Quintessence of Scaling Mana',
    'code' => 'greater_quintessence_of_scaling_mana',
  ),
  'greater_quintessence_of_scaling_mana_regeneration' => 
  array (
    'iconCode' => 'bl_4_3',
    'shortName' => 'Greater Quintessence of Scaling Mana Regeneration',
    'code' => 'greater_quintessence_of_scaling_mana_regeneration',
  ),
  'greater_quintessence_of_spell_vamp' => 
  array (
    'iconCode' => 'bl_4_3',
    'shortName' => 'Greater Quintessence of Spell Vamp',
    'code' => 'greater_quintessence_of_spell_vamp',
  ),
  'greater_quintessence_of_studio_rumble' => 
  array (
    'iconCode' => '8035',
    'shortName' => 'Greater Quintessence of Studio Rumble',
    'code' => 'greater_quintessence_of_studio_rumble',
  ),
  'greater_seal_of_ability_power' => 
  array (
    'iconCode' => '',
    'shortName' => 'Greater Seal of Ability Power',
    'code' => 'greater_seal_of_ability_power',
  ),
  'greater_seal_of_armor' => 
  array (
    'iconCode' => 'y_1_3',
    'shortName' => 'Greater Seal of Armor',
    'code' => 'greater_seal_of_armor',
  ),
  'greater_seal_of_attack_damage' => 
  array (
    'iconCode' => 'y_1_3',
    'shortName' => 'Greater Seal of Attack Damage',
    'code' => 'greater_seal_of_attack_damage',
  ),
  'greater_seal_of_attack_speed' => 
  array (
    'iconCode' => '',
    'shortName' => 'Greater Seal of Attack Speed',
    'code' => 'greater_seal_of_attack_speed',
  ),
  'greater_seal_of_cooldown_reduction' => 
  array (
    'iconCode' => 'y_1_3',
    'shortName' => 'Greater Seal of Cooldown Reduction',
    'code' => 'greater_seal_of_cooldown_reduction',
  ),
  'greater_seal_of_critical_chance' => 
  array (
    'iconCode' => '',
    'shortName' => 'Greater Seal of Critical Chance',
    'code' => 'greater_seal_of_critical_chance',
  ),
  'greater_seal_of_critical_damage' => 
  array (
    'iconCode' => 'y_1_3',
    'shortName' => 'Greater Seal of Critical Damage',
    'code' => 'greater_seal_of_critical_damage',
  ),
  'greater_seal_of_energy_regeneration' => 
  array (
    'iconCode' => 'y_1_3',
    'shortName' => 'Greater Seal of Energy Regeneration',
    'code' => 'greater_seal_of_energy_regeneration',
  ),
  'greater_seal_of_gold' => 
  array (
    'iconCode' => '',
    'shortName' => 'Greater Seal of Gold',
    'code' => 'greater_seal_of_gold',
  ),
  'greater_seal_of_health' => 
  array (
    'iconCode' => '',
    'shortName' => 'Greater Seal of Health',
    'code' => 'greater_seal_of_health',
  ),
  'greater_seal_of_health_regeneration' => 
  array (
    'iconCode' => 'y_1_3',
    'shortName' => 'Greater Seal of Health Regeneration',
    'code' => 'greater_seal_of_health_regeneration',
  ),
  'greater_seal_of_magic_resist' => 
  array (
    'iconCode' => 'y_3_3',
    'shortName' => 'Greater Seal of Magic Resist',
    'code' => 'greater_seal_of_magic_resist',
  ),
  'greater_seal_of_mana' => 
  array (
    'iconCode' => 'y_1_3',
    'shortName' => 'Greater Seal of Mana',
    'code' => 'greater_seal_of_mana',
  ),
  'greater_seal_of_mana_regeneration' => 
  array (
    'iconCode' => 'y_3_3',
    'shortName' => 'Greater Seal of Mana Regeneration',
    'code' => 'greater_seal_of_mana_regeneration',
  ),
  'greater_seal_of_percent_health' => 
  array (
    'iconCode' => 'y_3_3',
    'shortName' => 'Greater Seal of Percent Health',
    'code' => 'greater_seal_of_percent_health',
  ),
  'greater_seal_of_scaling_ability_power' => 
  array (
    'iconCode' => 'y_2_3',
    'shortName' => 'Greater Seal of Scaling Ability Power',
    'code' => 'greater_seal_of_scaling_ability_power',
  ),
  'greater_seal_of_scaling_armor' => 
  array (
    'iconCode' => 'y_2_3',
    'shortName' => 'Greater Seal of Scaling Armor',
    'code' => 'greater_seal_of_scaling_armor',
  ),
  'greater_seal_of_scaling_attack_damage' => 
  array (
    'iconCode' => 'y_4_3',
    'shortName' => 'Greater Seal of Scaling Attack Damage',
    'code' => 'greater_seal_of_scaling_attack_damage',
  ),
  'greater_seal_of_scaling_energy_regeneration' => 
  array (
    'iconCode' => 'y_3_3',
    'shortName' => 'Greater Seal of Scaling Energy Regeneration',
    'code' => 'greater_seal_of_scaling_energy_regeneration',
  ),
  'greater_seal_of_scaling_health' => 
  array (
    'iconCode' => 'y_2_3',
    'shortName' => 'Greater Seal of Scaling Health',
    'code' => 'greater_seal_of_scaling_health',
  ),
  'greater_seal_of_scaling_health_regeneration' => 
  array (
    'iconCode' => 'y_2_3',
    'shortName' => 'Greater Seal of Scaling Health Regeneration',
    'code' => 'greater_seal_of_scaling_health_regeneration',
  ),
  'greater_seal_of_scaling_magic_resist' => 
  array (
    'iconCode' => 'y_2_3',
    'shortName' => 'Greater Seal of Scaling Magic Resist',
    'code' => 'greater_seal_of_scaling_magic_resist',
  ),
  'greater_seal_of_scaling_mana' => 
  array (
    'iconCode' => 'y_2_3',
    'shortName' => 'Greater Seal of Scaling Mana',
    'code' => 'greater_seal_of_scaling_mana',
  ),
  'greater_seal_of_scaling_mana_regeneration' => 
  array (
    'iconCode' => 'y_2_3',
    'shortName' => 'Greater Seal of Scaling Mana Regeneration',
    'code' => 'greater_seal_of_scaling_mana_regeneration',
  ),
  'razer_mark_of_precision' => 
  array (
    'iconCode' => '10001',
    'shortName' => 'Razer Mark of Precision',
    'code' => 'razer_mark_of_precision',
  ),
);