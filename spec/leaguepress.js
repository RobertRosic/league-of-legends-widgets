describe("LeaguePress", function () {

    it("item tests",function(){
        var items = new ItemsBuild();
        items.addItem("dagger");
        expect(items.itemSets[0].currentItemIndex).toBe(1);

        items.addItem("dagger");
        expect(items.itemSets[0].currentItemIndex).toBe(2);

        items.addItem("boots_of_swiftness");
        items.addItemAtIndex("elixir_of_fortitude",0,5);

        var elixer = $.fn.LeaguePress.findByCode("items", "elixir_of_fortitude");
        elixer.amountPerSlot = 5;

        items.addItemAtIndex("elixir_of_fortitude",0,5);
        items.addItemAtIndex("elixir_of_fortitude",0,5);
        items.addItemAtIndex("elixir_of_fortitude",0,5);
        expect(items.itemSets[0].items[5].amount).toBe(4);

        items.addItemAtIndex("elixir_of_fortitude",0,5);
        items.addItemAtIndex("elixir_of_fortitude",0,5);
        items.addItemAtIndex("elixir_of_fortitude",0,5);
        // overflow without causing trouble
        expect(items.itemSets[0].items[5].amount).toBe(5);

        items.removeItemAtIndex(0,5);
        expect(items.itemSets[0].items[5].amount).toBe(4);

        items.removeItemAtIndex(0,5);
        items.removeItemAtIndex(0,5);
        items.removeItemAtIndex(0,5);
        items.removeItemAtIndex(0,5);
        items.removeItemAtIndex(0,5);
        expect(items.itemSets[0].items[5]).toBeNull();

        console.debug(JSON.stringify(items));
    });

    it("resources tests", function () {
        var champions = jQuery.fn.LeaguePress.champions;
        expect(champions).toBeDefined();
        expect(champions[0].code).toBe("aatrox");
    });

    it("champion tests", function () {
        var champion = new Champion();
        champion.setData(jQuery.fn.LeaguePress.champions[0]);

        var code = champion.getCode();
        expect(code).toEqual("aatrox");

        expect(function () {
            champion.addBuildAbility('omgwtfbbq', 1);
        }).toThrow();

        var ability = champion.getAbilityByCode('dark_flight');
        expect(ability.code).toEqual("dark_flight");

        champion.addBuildAbility('dark_flight', 1);

        var isValid = champion.addBuildAbility('dark_flight', 2);
        expect(isValid).toBeFalsy();

        champion.addBuildAbility('dark_flight', 4);
        expect(champion.build.abilities['dark_flight']).toEqual([1,4]);

        champion.removeBuildAbility('dark_flight', 1);
        expect(champion.build.abilities['dark_flight']).toEqual([4]);

        // just checking
        // console.debug(JSON.stringify(champion.build.abilities));
    });

    it("champion spells tests", function () {
        var spells = new ChampionSpells();

        expect(function () {
            spells.setSpell(2, 'smite');
        }).toThrow();

        spells.setSpell(0, 'smite');

        var isDuplicateSpellSet = spells.setSpell(1, 'smite');
        expect(isDuplicateSpellSet).toBeFalsy();

        spells.setSpell(1, 'ClairVoyance');

        var list = spells.getSpells();
        expect(list).toBeDefined();
        expect(list.length).toEqual(2);
        expect(list[0]).toEqual('smite');
        expect(list[1]).toEqual('clairvoyance');
    });

});

