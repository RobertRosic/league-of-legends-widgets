module.exports = function (grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg : grunt.file.readJSON('package.json'),
        uglify : {
            all : {
                options : {
                    banner : "/*! <%= pkg.name %> <%= grunt.template.today(\"yyyy-mm-dd\") %> */\n(function ($) {$(function(){\n",
                    footer : "\n});})(jQuery);\n"
                    // compress : false,
                    // mangle : false,
                    // beautify : true
                },
                files : {
                    'build/production/<%= pkg.name %>.min.js' : [
                        'src/<%= pkg.name %>.js',
                        'src/classes/*.js',
                        'src/ui/*.js'
                    ]
                }
            }
        },
        // https://github.com/gruntjs/grunt-contrib-jasmine
        jasmine : {
            src : [
                'src/leaguepress.js',
                'src/classes/*.js'
            ],
            options : {
                // host : 'http://agent1.example.nl/',
                '--web-security' : false,
                // '--local-to-remote-url-access' : true,
                // '--ignore-ssl-errors' : true,
                specs : 'spec/*.js',
                vendor : [
                    'src/vendor/jquery.min.js',
                    'src/vendor/jquery-ui.min.js',
                    'src/vendor/handlebars.min.js'
                ]
            }
        },
        jshint : {
            all : [
                'Gruntfile.js',
                'src/*.js',
                'src/ui/*.js',
                'src/classes/*.js',
                'spec/**/*.js'
            ],
            options : {
                jshintrc : '.jshintrc'
            }
        },
        less : {
            dev : {
                options : {},
                files : {
                    "build/development/leaguepress.css" : "src/css/default/leaguepress.less"
                }
            },
            prod : {
                options : {
                    yuicompress : true
                },
                files : {
                    "build/production/leaguepress.css" : "src/css/default/leaguepress.less"
                }
            }
        },
        handlebars : {
            all : {
                options : {
                    namespace : "LeaguePress",
                    processName : function (filePath) {
                        var pieces = filePath.split("/");
                        var fileName = pieces[pieces.length - 1];
                        return fileName.toLowerCase();
                    }
                },
                files : {
                    "build/production/leaguepress.handlebars.js" : ["src/templates/*.handlebars"]
                }
            }
        },
        copy : {
            prod : {
                files : [
                    {
                        src : ['build/production/*.js'],
                        dest : 'src/compiled/',
                        filter : 'isFile',
                        expand : true,
                        flatten : true
                    },
                    {
                        src : ['build/production/*.css', 'build/tmp/*.css'],
                        dest : 'src/compiled/default/',
                        filter : 'isFile',
                        expand : true,
                        flatten : true
                    },
                    {
                        src : 'build/production/leaguepress.min.js',
                        dest : 'src/wordpress/wp-content/plugins/leaguepress/javascript/',
                        filter : 'isFile',
                        expand : true,
                        flatten : true
                    },
                    {
                        src : 'src/vendor/handlebars.min.js',
                        dest : 'src/wordpress/wp-content/plugins/leaguepress/javascript/',
                        filter : 'isFile',
                        expand : true,
                        flatten : true
                    },
                    {
                        src : 'build/production/leaguepress.handlebars.js',
                        dest : 'src/wordpress/wp-content/plugins/leaguepress/javascript/',
                        filter : 'isFile',
                        expand : true,
                        flatten : true
                    },
                    {
                        src : 'build/production/leaguepress.css',
                        dest : 'src/wordpress/wp-content/plugins/leaguepress/css/default/',
                        filter : 'isFile',
                        expand : true,
                        flatten : true
                    },
                    {
                        src : ['src/library/*.json', 'src/library/*.jpeg', 'src/library/*.jpg', 'src/library/*.png'],
                        dest : 'src/wordpress/wp-content/plugins/leaguepress/library/',
                        filter : 'isFile',
                        expand : true,
                        flatten : true
                    },
                    {
                        src : 'src/images/*',
                        dest : 'src/wordpress/wp-content/plugins/leaguepress/images/',
                        filter : 'isFile',
                        expand : true,
                        flatten : true
                    },
                    {
                        src : 'src/fonts/*',
                        dest : 'src/wordpress/wp-content/plugins/leaguepress/fonts/',
                        filter : 'isFile',
                        expand : true,
                        flatten : true
                    }
                ]
            }
        },
        clean : {
            prod : ["build/production", "src/compiled"],
            dev : ["build/development", "src/compiled"]
        },
        watch : {
            javascript : {
                files : ['src/ui/*.js', 'src/classes/*.js'],
                tasks : ['build'],
                options : {
                    livereload : false
                }
            }
        }
    });

    // Load the plugins
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-jasmine');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    // https://github.com/gruntjs/grunt-contrib-less
    grunt.loadNpmTasks('grunt-contrib-less');
    // https://github.com/gruntjs/grunt-contrib-handlebars
    // npm ls | grep handlebars
    grunt.loadNpmTasks('grunt-contrib-handlebars');
    grunt.loadNpmTasks('grunt-contrib-watch');

    // Default task(s).
    grunt.registerTask('default', ['clean', 'jasmine', 'jshint', 'less', 'handlebars', 'uglify', 'copy']);
    grunt.registerTask('build', ['clean', 'less', 'handlebars', 'uglify', 'copy']);
};
