package {"ruby":}
package {"ruby-devel":}
package {"rubygems":
  require=>Package["ruby"]
}
package {"gcc":}
package {"make":}
package {"git":}
package {"httpd":}
package {"facter":}
package {"mysql":}
package {"mysql-server":}
package {"php.x86_64":
  require=>[Package["httpd"]]
}
package {"php-cli":}
package {"php-mysql":}

service {"httpd":
  ensure=>running,
  require=>Package["httpd"],
}

package {"hiera":
  require=>Package["rubygems"],
}

#package { 'librarian-puppet':
#  ensure   => 'installed',
#  provider => 'gem',
#  require=>Package["rubygems"],
#}

exec {"gem update --system":
  user=>"root",
  path=>["/usr/bin"],
  require=>Package["rubygems"],
}

file {"/etc/sysconfig/iptables":
  ensure=>present,
  source=>"/vagrant/puppet/files/iptables",
  group=>"root",
  owner=>"root",
}

file {"/home/vagrant/.bashrc":
  ensure=>present,
  source=>"/vagrant/puppet/files/.bashrc",
  group=>"root",
  owner=>"root",
}

file {"/etc/httpd/conf.d/leaguepress.conf":
  ensure=>present,
  source=>"/vagrant/puppet/files/leaguepress.conf",
  notify=>Service["httpd"],
  require=>Package["httpd"],
  group=>"root",
  owner=>"root",
}

service { "mysqld":
  ensure  => "running",
  enable  => "true",
  require => Package["mysql-server"],
}

file {"/etc/my.cnf":
  ensure=>present,
  source=>"/vagrant/puppet/files/my.cnf",
  notify=>Service["mysqld"],
  require=>Package["mysql-server"],
  group=>"root",
  owner=>"root",
}

service {"iptables":
  ensure=>"stopped",
}

exec {"updatedb":
  user=>"root",
  path=>["/usr/bin"],
}

# MySQL Access
exec { "/usr/bin/mysqladmin -u root password 'vagrant'":
  user=>"root",
  path=>["/usr/bin"],
  require=>[Package["mysql-server"],Package["mysql"]],
  returns=>[1,0],
} ->
exec {'mysql-grant-root':
  command=> "mysql --user=root --password=vagrant -e 'GRANT ALL PRIVILEGES ON *.* TO root@192.168.56.1 IDENTIFIED BY \"root\";'",
  path=>["/usr/bin"],
  require=>[Package["mysql-server"],Package["mysql"]],
  returns=>[0],
} ->

exec {'mysql-root-password-reset':
  command=> "mysql --user=root --password=vagrant -D mysql -e 'update user set password=password(\"vagrant\") where host=\"192.168.56.1\" and user=\"root\";'",
  path=>["/usr/bin"],
  require=>[Package["mysql-server"],Package["mysql"]],
  returns=>[0],
} ->

exec {'mysql-create-wordpress':
  command=> "mysql --user=root --password=vagrant -e 'create database if not exists wordpress';",
  path=>["/usr/bin"],
  require=>[Package["mysql-server"],Package["mysql"]],
  returns=>[0],
} ->

exec {'mysql-import-wordpress':
  command=> "mysql --user=root --password=vagrant -D wordpress < /vagrant/puppet/files/wordpress.sql",
  path=>["/usr/bin"],
  require=>[Package["mysql-server"],Package["mysql"]],
  returns=>[0],
} ->

exec {'mysql-flush-privileges':
  command=> "mysqladmin --user=root --password=vagrant flush-privileges",
  path=>["/usr/bin"],
  require=>[Package["mysql-server"],Package["mysql"]],
  returns=>[1,0],
}

# puppet-librarian install
# sudo puppet apply ./manifests/modules.pp --modulepath=./modules/
# sudo /usr/local/bin/npm install -g grunt-cli

