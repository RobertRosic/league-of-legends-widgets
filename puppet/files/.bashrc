# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
        . /etc/bashrc
fi

# User specific aliases and functions

#alias puppet-master='sudo puppet agent --server=puppetmaster.frisket.nl --no-daemonize --verbose --test'
#alias npm-i='sudo /usr/local/bin/npm install'

cd /vagrant